#!/bin/bash
# link files inside that directory to dotfiles in /home

read -p "This script will OVERWRITE your /home's dotfiles. Type uppercase YES to continue: "
echo

if [[ "$REPLY" != "YES" ]];
then
    echo "Didn't write anything. Exiting."
    exit 1
fi

SCRIPTNAME=`basename $0`
pushd `dirname $0` > /dev/null
DIRPATH=`pwd -P`
popd > /dev/null

for filename in *;
do
    if [[ "$filename" != "$SCRIPTNAME" ]] && [[ "$filename" != "README.txt" ]];
    then
        COMMAND="ln -sf $DIRPATH/$filename $HOME/.$filename"
        echo "$COMMAND"
        `$COMMAND`
    fi
done
