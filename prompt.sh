# two below functions based on https://github.com/jimeh/git-aware-prompt

get_git_branch() {
  # Based on: http://stackoverflow.com/a/13003854/170413
  local branch
  if branch=$(git rev-parse --abbrev-ref HEAD 2> /dev/null); then
    if [[ "$branch" == "HEAD" ]]; then
      branch='detached*'
    fi
    branch="$branch"
  else
    branch=""
  fi
  echo "$branch"
}

get_git_dirty() {
  local git_dirty
  local status=$(git status --porcelain 2> /dev/null)
  if [[ "$status" != "" ]]; then
    git_dirty='*'
  else
    git_dirty=''
  fi
  echo "$git_dirty"
}

##########TEST
shorten_path() {
    local arr=($(echo $1 | tr "/" " "))
    if [ ${#arr[@]} -lt 2 ]; then
        echo "$1"
        exit 0
    fi
    local last=${arr[${#arr[@]} - 1]}
    last=${last:0:10}
    unset arr[${#arr[@]}-1]  # remove last element
    local ret=""
    for x in ${arr[@]}
    do
        ret+="/${x:0:1}"
    done
    ret+="/$last"
    echo "$ret"
}

set_path_short() {
    if [ "$PWD" == "$HOME" ]; then
        SP="~"
    else
        SP=$(shorten_path $PWD)
    fi
}

set_git_path() {
    GP=$(get_git_branch)
    if [ $GP ]; then
        GP="($(shorten_path $GP))"
    fi
}

set_git_dirty() {
    GD=$(get_git_dirty)
}


PROMPT_COMMAND="set_git_path; set_git_dirty; set_path_short; $PROMPT_COMMAND"


# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi
 
# boogi: I set "xterm" in putty; "screen" is sometimes (rarely) set when in screen/tmux/byobu
if [ "$TERM" = xterm ] || [ "$TERM" = screen ]; then
    TERM=xterm-256color
fi

# i always use it and don't care for old weird systems
color_prompt=yes
# will work with "sudo -s"
source ~/.colors.sh

# UserColor - root is red, and casual user has casual color
if [ $(id -u) -eq 0 ];
then
    UC=$bldred
else
    UC=$bldgrn
fi

if [ "$color_prompt" = yes ]; then
    PS1="${debian_chroot:+($debian_chroot)}"
    PS1+="\[$UC\]\u\[$bldgrn\]@\h\[$bldblk\]:\[$bldblu\]\$SP"
    PS1+=" \[$bldylw\]\$GP\[$txtcyn\]\$GD\[$txtrst\]\$ "
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt UC

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

export PS1
